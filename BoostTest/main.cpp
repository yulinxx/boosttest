// client.cpp : 定义控制台应用程序的入口点。
//

//
// chat_client.cpp
// ~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <deque>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include "chat_message.cpp"

using boost::asio::ip::tcp;
using namespace std;
typedef std::deque<chat_message> chat_message_queue;


//////////////////////////////////////////////////////////////////////////
class chat_client
{
public:
	chat_client(boost::asio::io_service& io_service, // 1
		tcp::resolver::iterator endpoint_iterator)
		: m_io_service(io_service),
		m_socket(io_service)
	{
		cout << __FUNCTION__ << endl;
		tcp::endpoint endpoint = *endpoint_iterator;

		m_socket.async_connect(endpoint,
			boost::bind(&chat_client::handle_connect, this, // 2
				boost::asio::placeholders::error, ++endpoint_iterator));
	}

	void write(const chat_message& msg) // 5
	{
		cout << __FUNCTION__ << endl;
		m_io_service.post(boost::bind(&chat_client::do_write, this, msg));
	}

	void close()
	{
		cout << __FUNCTION__ << endl;
		m_io_service.post(boost::bind(&chat_client::do_close, this));
	}

private:

	void handle_connect(const boost::system::error_code& error, // 2
		tcp::resolver::iterator endpoint_iterator)
	{
		cout << __FUNCTION__ << endl;
		if (!error)
		{
			boost::asio::async_read(m_socket,
				boost::asio::buffer(m_read_msg.data(), chat_message::header_length),  //copy buffer to read_msg_'s header
				boost::bind(&chat_client::handle_read_header, this, // 3
					boost::asio::placeholders::error));
		}
		else if (endpoint_iterator != tcp::resolver::iterator())
		{
			m_socket.close();
			tcp::endpoint endpoint = *endpoint_iterator;
			m_socket.async_connect(endpoint,
				boost::bind(&chat_client::handle_connect, this, // 2
					boost::asio::placeholders::error, ++endpoint_iterator));
		}
	}

	void handle_read_header(const boost::system::error_code& error) // 3
	{
		cout << __FUNCTION__ << endl;
		if (!error && m_read_msg.decode_header())
		{
			boost::asio::async_read(m_socket,
				boost::asio::buffer(m_read_msg.body(), m_read_msg.body_length()), //copy buffer to read_msg_'s body
				boost::bind(&chat_client::handle_read_body, this, // 4
					boost::asio::placeholders::error));
		}
		else
		{
			do_close();
		}
	}

	void handle_read_body(const boost::system::error_code& error) // 4
	{
		cout << __FUNCTION__ << endl;
		if (!error)
		{
			std::cout.write(m_read_msg.body(), m_read_msg.body_length()); // print read_msg_'s body

			std::cout << "\n";
			boost::asio::async_read(m_socket,
				boost::asio::buffer(m_read_msg.data(), chat_message::header_length),
				boost::bind(&chat_client::handle_read_header, this, // 4
					boost::asio::placeholders::error));
		}
		else
		{
			do_close();
		}
	}


	void do_write(chat_message msg) // 6
	{
		cout << __FUNCTION__ << endl;
		bool write_in_progress = !m_write_msgs.empty();
		m_write_msgs.push_back(msg);
		if (!write_in_progress)
		{
			boost::asio::async_write(m_socket,
				boost::asio::buffer(m_write_msgs.front().data(),
					m_write_msgs.front().length()), // copy write_msgs_.front() to buffer
				boost::bind(&chat_client::handle_write, this, // 7 send message
					boost::asio::placeholders::error));
		}
	}


	void handle_write(const boost::system::error_code& error) // 7
	{
		cout << __FUNCTION__ << endl;
		if (!error)
		{
			m_write_msgs.pop_front();
			if (!m_write_msgs.empty())
			{
				boost::asio::async_write(m_socket,
					boost::asio::buffer(m_write_msgs.front().data(),
						m_write_msgs.front().length()),
					boost::bind(&chat_client::handle_write, this, // 7
						boost::asio::placeholders::error));
			}
		}
		else
		{
			do_close();
		}
	}

	void do_close()
	{
		cout << __FUNCTION__ << endl;
		m_socket.close();
	}

private:
	boost::asio::io_service& m_io_service;
	tcp::socket m_socket;
	chat_message m_read_msg;  // 存从buffer读出的数据
	chat_message_queue m_write_msgs; // 欲写入buffer的数据队列，deque
};



//////////////////////////////////////////////////////////////////////////
int main()
{
	// 调用栈：
	// 打开客户端并连接（聊天室没有对话时）：1-2
	// 打开客户端并连接（聊天室有对话时）:1-4
	// 自己发言:56734
	// 聊天室内其他成员发言：34
	try
	{
		boost::asio::io_service io_service;

		tcp::resolver resolver(io_service);
		tcp::resolver::query query("127.0.0.1", "1000"); // ip port:本机
		tcp::resolver::iterator iterator = resolver.resolve(query);

		chat_client c(io_service, iterator); // 初始化、连接

		boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service)); // 线程

		char line[chat_message::max_body_length + 1];

		while (std::cin.getline(line, chat_message::max_body_length + 1))
		{
			chat_message msg;
			msg.body_length(strlen(line));
			memcpy(msg.body(), line, msg.body_length());// line to msg
			msg.encode_header();
			c.write(msg);
		}

		c.close();
		t.join(); // 执行线程
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}