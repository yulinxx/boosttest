//
// chat_message.hpp
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
#pragma warning(disable:4996)
#ifndef CHAT_MESSAGE_HPP
#define CHAT_MESSAGE_HPP

#include <cstdio>
#include <cstdlib>
#include <cstring>

class chat_message
{
public:
	enum { header_length = 4 };
	enum { max_body_length = 512 };

	chat_message()
		: m_body_length(0)
	{
	}

	const char* data() const
	{
		return m_data;
	}

	char* data()
	{
		return m_data;
	}

	size_t length() const
	{
		return header_length + m_body_length;
	}

	const char* body() const
	{
		return m_data + header_length;
	}

	char* body()
	{
		return m_data + header_length;
	}

	size_t body_length() const
	{
		return m_body_length;
	}

	void body_length(size_t length)
	{
		m_body_length = length;
		if (m_body_length > max_body_length)
			m_body_length = max_body_length;
	}

	bool decode_header()
	{
		using namespace std; // For strncat and atoi.
		char header[header_length + 1] = "";
		strncat(header, m_data, header_length);
		m_body_length = atoi(header);
		if (m_body_length > max_body_length)
		{
			m_body_length = 0;
			return false;
		}
		return true;
	}

	void encode_header()
	{
		using namespace std; // For sprintf and memcpy.
		char header[header_length + 1] = "";
		sprintf(header, "%4d", m_body_length);
		memcpy(m_data, header, header_length);
	}

private:
	char m_data[header_length + max_body_length];
	size_t m_body_length;
};

#endif // CHAT_MESSAGE_HPP